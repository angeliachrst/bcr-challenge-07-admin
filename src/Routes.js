import { BrowserRouter, Routes, Route } from 'react-router-dom';
import App from './App';
import Register from './Register';
import LandingPage from './Pages/User/LandingPage';
import CariMobil from './Pages/User/CariMobil';
import HasilPencarian from './Pages/User/HasilPencarian';
import DetailMobil from './Pages/User/DetailMobil';
import Dashboard from './Pages/Admin/Dashboard';
import Cars from './Pages/Admin/Cars';
import AddNewCar from './Pages/Admin/AddNewCar';

const RouteApp = () => {

    return (
        <>
        <BrowserRouter>
        <Routes>
            <Route path="/" element={<App />} />
            <Route path="/landingpage" element={<LandingPage />} />
            <Route path="/carimobil" element={<CariMobil />} />
            <Route path="/hasilpencarian" element={<HasilPencarian />} />
            <Route path="/detailmobil/:idCar" element={<DetailMobil />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/cars" element={<Cars />} />
            <Route path="/addnewcar" element={<AddNewCar />} />
            <Route path="/register" element={<Register />} />
            <Route path="*" element={<h1 style={{ fontSize: "48px", textAlign: "center", paddingTop: "250px" }}>Error 404</h1>} />
        </Routes>
        </BrowserRouter>
        </>
    )
}

export default RouteApp;