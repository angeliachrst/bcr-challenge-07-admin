import Navbar from '../../Components/Admin/Navbar';
import DashContent from '../../Components/Admin/DashContent';
import { carAdminAction } from '../../Config/Redux/Actions/carAdminAction';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

const Dashboard = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const { dataLogin } = useSelector((state) => state.auth)
    dispatch(carAdminAction());

    useEffect(() => {
        if(dataLogin?.email !== "admin@admin.com") navigate('/');
        document.body.style.background = "white";
        document.title = "Dashboard";
        document.getElementsByClassName("dash-active")[0].style.fontWeight = 700;
        document.getElementsByClassName("dash-active")[0].style.background = "rgba(255, 255, 255, 0.3)";
        //eslint-disable-next-line
    }, [])
    
    return(
        <>
        <Navbar />
        <DashContent />
        </>
    )
}

export default Dashboard;