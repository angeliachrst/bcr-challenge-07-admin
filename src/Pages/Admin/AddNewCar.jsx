import Navbar from '../../Components/Admin/Navbar';
import AddNewCarContent from '../../Components/Admin/AddNewCarContent';
import { useEffect } from 'react';

const AddNewCar = () => {
    useEffect(() => {
        document.getElementsByClassName("sideText1")[0].textContent = "CARS";
        document.getElementsByClassName("sideText2")[0].textContent = "List Car";
        document.getElementsByClassName("cars-active")[0].style.fontWeight = 700;
        document.getElementsByClassName("cars-active")[0].style.background = "rgba(255, 255, 255, 0.3)";
        document.title = "Add New Car";
    }, [])

    return (
        <>
        <Navbar />
        <AddNewCarContent />
        </>
    )
}

export default AddNewCar;