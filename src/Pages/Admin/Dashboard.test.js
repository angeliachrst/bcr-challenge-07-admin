import { render, screen } from "@testing-library/react";
import Dashboard from "./Dashboard";
import { Provider } from "react-redux";
import configureStore from 'redux-mock-store';
import { BrowserRouter } from 'react-router-dom';

it("Should be display text Dashboard", () => {
    const initialState = { dataLogin: null };
    const mockConfigStore = configureStore();
    const mockStore = mockConfigStore(initialState);

    render(
        <BrowserRouter>
            <Provider store={mockStore}>
                <Dashboard />
            </Provider>
        </BrowserRouter>
    );

    const titlePage = screen.getByText("Dashboard");
    expect(titlePage).toBeInTheDocument();
});
