import Navbar from '../../Components/Admin/Navbar';
import CarsContent from '../../Components/Admin/CarsContent';
import { useEffect } from 'react';

const Cars = () => {
    useEffect(() => {
        document.getElementsByClassName("sideText1")[0].textContent = "CARS";
        document.getElementsByClassName("sideText2")[0].textContent = "List Car";
        document.getElementsByClassName("cars-active")[0].style.fontWeight = 700;
        document.getElementsByClassName("cars-active")[0].style.background = "rgba(255, 255, 255, 0.3)";
        document.title = "Cars";
    }, [])
    
    return (
        <>
        <Navbar />
        <CarsContent />
        </>
    )
}

export default Cars;