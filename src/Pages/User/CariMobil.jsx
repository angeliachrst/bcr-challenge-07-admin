import Header from "../../Components/User/Header";
import Footer from "../../Components/User/Footer";
import CardCariMobil from "../../Components/User/CardCariMobil";
import { useEffect } from "react";

const CariMobil = () => {
    useEffect(() => {
        document.getElementsByClassName("btn-hero")[0].style.display = "none";
        document.title = "Cari Mobil";
    }, []);

    return (
        <>
        <Header />
        <CardCariMobil />
        <Footer />
        </>
    );
};

export default CariMobil;