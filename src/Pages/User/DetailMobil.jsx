import Header from "../../Components/User/Header";
import Footer from "../../Components/User/Footer";
import CardCariInactive from "../../Components/User/CardCariInactive";
import DetailCar from "../../Components/User/DetailCar";
import { useEffect } from "react";

const DetailMobil = () => {
    useEffect(() => {
        document.getElementsByClassName("hero")[0].style.display = "none";
        document.getElementsByClassName("btn-hero")[0].style.display = "none";
        document.getElementsByClassName("header")[0].style.height = "266px";
        document.getElementById("footer").style.marginTop = "890px";
        document.title = "Detail Mobil";
    }, []);

    return (
        <>
        <Header />
        <CardCariInactive />
        <DetailCar />
        <Footer />
        </>
    )
};

export default DetailMobil;