import Header from "../../Components/User/Header";
import Footer from "../../Components/User/Footer";
import CardHasilPencarian from "../../Components/User/CardHasilPencarian";
import ListCar from "../../Components/User/ListCar";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { carAdminAction } from "../../Config/Redux/Actions/carAdminAction";

const HasilPencarian = () => {
    const dispatch = useDispatch();
    dispatch(carAdminAction());
    useEffect(() => {
        document.getElementsByClassName("hero")[0].style.display="none";
        document.getElementsByClassName("btn-hero")[0].style.display = "none";
        document.getElementsByClassName("header")[0].style.height = "266px";
        document.getElementById("footer").style.marginTop = "100px";
        document.title = "Hasil Pencarian";
    }, []);

    return (
        <>
        <Header />
        <CardHasilPencarian />
        <ListCar />
        <Footer />
        </>
    );
};

export default HasilPencarian;