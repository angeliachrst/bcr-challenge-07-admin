import "./CarsContent.css";
import { useSelector } from "react-redux";

const CarsContent = () => {
  const { dataCarAdmin } = useSelector(
    (globalStore) => globalStore.carAdminReducer
  );
  const rupiah = (number) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(number);
  }

  return (
    <div className="content">
      <div className="breadcrumb">
        <h1>Cars</h1>
        <img src="img/arrow-right.svg" alt="arrow-right" />
        <p>List Car</p>
      </div>

      <div className="title-button-car">
        <h1 className="title-h1">List Car</h1>
        <a href="/addnewcar">
          <img src="/img/add-new-car.svg" alt="add-new-car" />
        </a>
      </div>

      <div className="button-category">
        <img src="/img/button-all.svg" alt="button-all" />
        <img src="/img/button-small.svg" alt="button-small" />
        <img src="/img/button-medium.svg" alt="button-medium" />
        <img src="/img/button-large.svg" alt="button-large" />
      </div>

      <div className="cars-table">
        {dataCarAdmin?.map((car) => (
          <div key={car.id} className="cars-card card">
            <img src="/img/car.png" className="card-img-top" alt="car" />
            <div className="card-body">
              <h1>{car.name}</h1>
              <h1 className="bold-h1">{`${rupiah(car.price)} / hari`}</h1>

              <div className="card-body-text">
                <img src="/img/key.svg" alt="key" />
                <h1>{car.start_rent_at} - {car.finish_rent_at}</h1>
              </div>

              <div className="card-body-text">
                <img src="/img/clock.svg" alt="clock" />
                <h1>{car.updatedAt}</h1>
              </div>

              <div className="button-card">
                <img className="delete" src="/img/delete.svg" alt="delete" />
                <a href="/addnewcar">
                  <img src="/img/edit.svg" alt="edit" />
                </a>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CarsContent;
