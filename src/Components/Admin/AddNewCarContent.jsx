import "./AddNewCarContent.css";
import { useNavigate } from 'react-router-dom';

const AddNewCarContent = () => {
  const navigate = useNavigate();
  const handleSaveData = () => {
    alert("Data Berhasil Disimpan");
    navigate("/cars");
  };
  return (
    <>
      <div className="content">
        <div className="breadcrumb">
          <h1>Cars</h1>
          <img src="img/arrow-right.svg" alt="arrow-right" />
          <h1>List Car</h1>
          <img src="img/arrow-right.svg" alt="arrow-right" />
          <p>Add New Car</p>
        </div>
        <h1 className="title-h1">Add New Car</h1>

        {/* form */}
        <div className="form-add-new-car">
          <div className="content-form row g-2 align-items-center">
            <div className="col-auto">
              <label htmlFor="nama" className="col-form-label">
                Nama<b style={{ color: "red" }}>*</b>
              </label>
            </div>
            <div className="content-box col-auto">
              <input
                type="name"
                id="nama"
                className="form-control"
                name="nama"
                placeholder="Nama"
              />
            </div>
          </div>
          <div className="content-form row g-2 align-items-center">
            <div className="col-auto">
              <label htmlFor="harga" className="col-form-label">
                Harga<b style={{ color: "red" }}>*</b>
              </label>
            </div>
            <div className="content-box col-auto">
              <input
                type="text"
                id="harga"
                className="form-control"
                name="harga"
                placeholder="Harga"
              />
            </div>
          </div>
          <div className="content-form row g-2 align-items-center">
            <div className="col-auto">
              <label htmlFor="foto" className="col-form-label">
                Foto<b style={{ color: "red" }}>*</b>
              </label>
            </div>
            <div className="content-box col-auto">
              <input
                type="file"
                id="harga"
                className="form-control"
                name="foto"
                placeholder="Foto"
              />
              <small className="form-text text-muted text-left">
                File size max. 2MB
              </small>
            </div>
          </div>
          <div className="content-h1">
            <h1>Start Rent</h1>
            <h1>-</h1>
          </div>
          <div className="content-h1">
            <h1>Finish Rent</h1>
            <h1>-</h1>
          </div>
          <div className="content-h1">
            <h1>Created at</h1>
            <h1>-</h1>
          </div>
          <div className="content-h1">
            <h1>Updated at</h1>
            <h1>-</h1>
          </div>
        </div>

        {/* button */}
        <div className="button-form">
          <a href="/cars">
            <img src="/img/cancel.svg" alt="cancel" />
          </a>
          <a href="/cars" onClick={handleSaveData}>
            <img src="/img/save.svg" alt="save" />
          </a>
        </div>
      </div>
    </>
  );
};

export default AddNewCarContent;
